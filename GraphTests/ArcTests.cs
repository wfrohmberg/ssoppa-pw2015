﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Graph;

namespace GraphTests
{
    [TestClass]
    public class ArcTests
    {
        Arc<string, int> arc;
        DiGraph<string, int> g;
        [TestInitialize()]
        public void Initialize()
        {
            g = new DiGraph<string, int>();
            arc = new Arc<string, int>(5);
        }

        [TestMethod]
        public void initalizeNode()
        {
            Assert.IsNotNull(arc);
        }

        [TestMethod]
        public void setArcValueSetsArcValue()
        {
            arc.setArcValue(100);
            Assert.AreEqual(arc.getArcValue(), 100);
        }

        [TestMethod]
        public void setNodeFromSetsNode()
        {
            Node<string,int> temp = new Node<string, int>(0,"test");
            arc.setNodeFrom(temp);
            Assert.AreEqual(arc.getNodeFrom(), temp);
        }

        [TestMethod]
        public void setNodeToSetsNode()
        {
            Node<string, int> temp = new Node<string, int>(0, "test");
            arc.setNodeTo(temp);
            Assert.AreEqual(arc.getNodeTo(), temp);
        }
    }
}
