﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Graph;

namespace GraphTests
{
    [TestClass]
    public class NodeTests
    {
        Node<string, int> node;
        [TestInitialize()]
        public void Initialize()
        {
            DiGraph<string, int> g = new DiGraph<string, int>();
            node = new Node<string, int>(0, "test");
        }

        [TestMethod]
        public void initalizeNode()
        {
            Assert.IsNotNull(node);
        }

        [TestMethod]
        public void getNodeValueReturnsProperValue()
        {
            Assert.AreEqual(node.getNodeValue(), "test");
        }

        [TestMethod]
        public void setNodeValueSetsProperValue()
        {
            node.setNodeValue("test2");
            Assert.AreEqual(node.getNodeValue(), "test2");
        }

        [TestMethod]
        public void equalNodeIsReturnTrue()
        {
            Node<string, int> temp = node;
            Assert.IsTrue(node.Equals(temp));
        }

        [TestMethod]
        public void nodeComparedReturnProperValue()
        {
            Node<string, int> temp = new  Node<string, int>(node);
            node.setIndex(1);
            Assert.AreEqual(node.CompareTo(temp), 1);
            temp.setIndex(2);
            Assert.AreEqual(node.CompareTo(temp), -1);
        }

        [TestMethod]
        public void getArcsReturnsCurrentListOfArcs()
        {
            DiGraph<string, int> g = new DiGraph<string, int>();
            g[0] = "test";
            g[1] = "test";
            g[0, 1] = 1;
            Assert.AreEqual(g[0].getArcs()[0], g[0,1]);
        }

        [TestMethod]
        public void getIndexReturnsCurrentIndex()
        {
            Assert.AreEqual(node.getIndex(), 0);
        }

        [TestMethod]
        public void addToArcsAddArcToList()
        {
            Arc<string, int> temp = new Arc<string, int>(5);
            node.AddToArcs(temp);
            Assert.AreEqual(node.getArcs()[0], temp);
        }

        [TestMethod]
        public void assignToStringChangesItsValue()
        {
            node = "hello";
            Assert.AreEqual(node.getNodeValue(), "hello");
        }
    }
}
