﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Graph;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GraphTests
{
    [TestClass]
    public class GraphStructureTests
    {
        DiGraph<string, int> g;
        [TestInitialize()]
        public void Initialize() 
        {
            g = new DiGraph<string, int>();
            g[0] = "test";
        }

        [TestMethod]
        public void initializeGraph()
        {
            Assert.IsNotNull(g);
        }

        [TestMethod]
        public void addNodeReturnsNotNullObject()
        {
            Assert.IsNotNull(g[0]);
        }

        [TestMethod]
        public void addNodeIncrementsNodesList()
        {
            Assert.AreEqual(g.numberOfNodes(), 1);
            g[1] = "test";
            Assert.AreEqual(g.numberOfNodes(), 2);
        }

        [TestMethod]
        public void addNodeSaveNodeValue()
        {
            Assert.AreEqual(g[0].ToString(), "test");
        }

        [TestMethod]
        public void addNodeSaveNodeIndex()
        {
            Assert.AreEqual(g[0].getIndex(), 0);
        }

        [TestMethod]
        public void addExistingNodeChangesItsValue()
        {
            Assert.AreEqual(g[0].ToString(), "test");
            g[0] = "test2";
            Assert.AreEqual(g[0].ToString(), "test2");
        }

        [TestMethod]
        public void deleteNodeRemovesNodeFromList()
        {
            Assert.AreEqual(g.numberOfNodes(), 1);
            g = g - g[0];
            Assert.AreEqual(g.numberOfNodes(), 0);
        }

        [TestMethod]
        public void addArcReturnsNotNullObject()
        {
            g[1] = "test2";
            g[0,1] = 1;
            Assert.IsNotNull(g[0,1]);
        }

        [TestMethod]
        public void addArcIncrementsNodesList()
        {
            g[1] = "test2";
            g[0, 1] = 1;
            Assert.AreEqual(g.numberOfArcs(), 1);
            g[1, 0] = 1;
            Assert.AreEqual(g.numberOfArcs(), 2);
        }

        [TestMethod]
        public void addArcSaveArcValue()
        {
            g[1] = "test2";
            g[0, 1] = 1;
            Assert.AreEqual(g[0,1].getArcValue(), 1);
        }

        [TestMethod]
        public void addExistingArcChangesItsValue()
        {
            g[1] = "test2";
            g[0, 1] = 1;
            Assert.AreEqual(g[0, 1].getArcValue(), 1);
            g[0, 1] = 2;
            Assert.AreEqual(g[0, 1].getArcValue(), 2);
        }

        [TestMethod]
        public void addArcSavesNodesInArc()
        {
            g[1] = "test2";
            g[0, 1] = 1;
            Assert.AreEqual(g[0, 1].getNodeFrom(), g[0]);
            Assert.AreEqual(g[0, 1].getNodeTo(), g[1]);
        }

        [TestMethod]
        public void addArcSavesArcInNode()
        {
            g[1] = "test2";
            g[0, 1] = 1;
            Assert.AreEqual(g[0].getArcs()[0], g[0, 1]);
        }

        [TestMethod]
        public void deleteArcRemovesArcFromList()
        {
            g[1] = "test2";
            g[0, 1] = 1;
            Assert.AreEqual(g.numberOfArcs(), 1);
            g = g - g[0, 1];
            Assert.AreEqual(g.numberOfArcs(), 0);
        }

        [TestMethod]
        public void rebuildSortsArcsInNodes()
        {
            g[1] = "test2";
            g[2] = "test2";
            g[3] = "test2";
            g[0, 1] = 1;
            g[0, 3] = 5;
            g[0, 2] = 3;
            List<Arc<string, int>> temp = g[0].getArcs();
            g.Rebuild();
            temp.Sort();
            Assert.AreEqual(temp, g[0].getArcs());
        }

        [TestMethod]
        public void nodesWhereReturnsNodesSpecifiedInPredicate()
        {
            g[1] = "test2";
            g[2] = "test2";
            g[3] = "test2";
            Predicate<Node<string, int>> predicate = x => x.getIndex() < 2;
            List<Node<string, int>> temp = new List<Node<string, int>>();
            temp.Add(g[0]); temp.Add(g[1]);
            bool equal = g.NodesWhere(predicate).ToList().SequenceEqual(temp);
            Assert.IsTrue(equal);
        }
        
        [TestMethod]
        public void arcsWhereReturnsArcsSpecifiedInPredicate()
        {
            g[1] = "test2";
            g[2] = "test2";
            g[3] = "test2";
            g[0, 1] = 1;
            g[0, 3] = 5;
            g[0, 2] = 3;
            Predicate<Arc<string, int>> predicate = x => x.getArcValue() < 5;
            List<Arc<string, int>> temp = new List<Arc<string, int>>();
            temp.Add(g[0, 1]); temp.Add(g[0, 2]);
            bool equal = g.ArcsWhere(predicate).ToList().SequenceEqual(temp);
            Assert.IsTrue(equal);
        }
    }
}
