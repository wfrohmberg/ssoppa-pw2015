﻿using System;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Graph
{
    [DataContract]
    public class DiGraph<NodeType, ArcType> : DiGraphInterface<NodeType, ArcType>
    {
        public List<Node<NodeType, ArcType>> nodes;
        public Dictionary<int, List<Arc<NodeType, ArcType>>> arcs;

        [DataMember]
        public List<Node<NodeType, ArcType>> Nodes
        {
            get { return nodes; }
            set { nodes = value; }
        }

        [DataMember]
        public Dictionary<int, List<Arc<NodeType, ArcType>>> Arcs
        {
            get { return arcs; }
            set { arcs = value; }
        }

        public DiGraph()
        {
            nodes = new List<Node<NodeType, ArcType>>();
            arcs = new Dictionary<int, List<Arc<NodeType, ArcType>>>();
        }
        public Node<NodeType, ArcType> this[int index]
        {
            get
            {
                return this.nodes[index];
            }
            set
            {
                if (nodes.ElementAtOrDefault(index) != null)
                {
                    this.nodes[index].setNodeValue(value);
                }
                else
                {
                    this.nodes.Insert(index, new Node<NodeType, ArcType>(index, value));
                }
            }
        }

        public Arc<NodeType, ArcType> this[int index1, int index2]
        {
            get
            {
                return this.arcs[index1].Find(x => x.getNodeTo().Equals(nodes[index2]));
            }
            set
            {
                try
                {
                    this.arcs[index1].Find(x => x.getNodeTo().Equals(nodes[index2])).setArcValue(value);
                }
                catch (Exception)
                {
                    if(!this.arcs.ContainsKey(index1))
                    {
                        this.arcs.Add(index1, new List<Arc<NodeType,ArcType>>());
                    }
                    Arc<NodeType, ArcType> arc = new Arc<NodeType, ArcType>(value, nodes[index1], nodes[index2]);
                    this.arcs[index1].Add(arc);
                    nodes[index1].AddToArcs(arc);
                }
            }
        }

        private void deleteNode(Node<NodeType, ArcType> node)
        {
            this.nodes.Remove(node);
        }

        private void deleteArc(Arc<NodeType, ArcType> arc)
        {
            foreach(KeyValuePair<int, List<Arc<NodeType, ArcType>>> temp in arcs)
                if(temp.Value.Contains(arc))
                    temp.Value.Remove(arc);
        }

        public int numberOfNodes()
        {
            return this.nodes.Count();
        }

        public int numberOfArcs()
        {
            int counter = 0;
            foreach (KeyValuePair<int, List<Arc<NodeType, ArcType>>> temp in arcs)
                counter += temp.Value.Count;
            return counter;
        }

        public void printArcs()
        {
            foreach (KeyValuePair<int, List<Arc<NodeType, ArcType>>> temp in arcs)
                foreach (Arc<NodeType, ArcType> temp_arc in temp.Value)
                    Console.WriteLine("From " + temp_arc.getNodeFrom().getIndex() + " to " + temp_arc.getNodeTo().getIndex());
        }

        public static DiGraph<NodeType, ArcType> operator- (DiGraph<NodeType, ArcType> graph, Node<NodeType, ArcType> node)
        {
            graph.deleteNode(node);
            return graph;
        }

        public static DiGraph<NodeType, ArcType> operator -(DiGraph<NodeType, ArcType> graph, Arc<NodeType, ArcType> arc)
        {
            graph.deleteArc(arc);
            return graph;
        }

        public IEnumerable<Node<NodeType, ArcType>> NodesWhere(Predicate<Node<NodeType, ArcType>> predicate)
        {
            return new List<Node<NodeType, ArcType>>(nodes.FindAll(predicate));
        }
        public IEnumerable<Arc<NodeType, ArcType>> ArcsWhere(Predicate<Arc<NodeType, ArcType>> predicate)
        {
            List<Arc<NodeType, ArcType>> temp_list = new List<Arc<NodeType, ArcType>>();
            foreach (KeyValuePair<int, List<Arc<NodeType, ArcType>>> temp in arcs)
                temp_list.AddRange(temp.Value.FindAll(predicate));
            return temp_list;
        }
        public void Rebuild()
        {
            foreach (KeyValuePair<int, List<Arc<NodeType, ArcType>>> temp in arcs)
                temp.Value.Sort();
            foreach (Node<NodeType, ArcType> temp in nodes)
                temp.getArcs().Sort();
        }

        public void Add(object o1, object o2)
        {
            this.nodes.Add(o1 as Node<NodeType, ArcType>);
            this.arcs.Add(0, o2 as List<Arc<NodeType, ArcType>>);
        }

        public static DiGraph<NodeType, ArcType> xmlToGraph(string file)
        {
            DataContractSerializer dcs = new DataContractSerializer(typeof(DiGraph<NodeType, ArcType>));
            FileStream fs = new FileStream(file, FileMode.Open);
            XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());

            DiGraph<NodeType, ArcType> graph = (DiGraph<NodeType, ArcType>)dcs.ReadObject(reader);
            reader.Close();
            fs.Close();
            return graph;
        }

        public static void serializeToXml(string file, DiGraph<NodeType, ArcType> config)
        {
            var ds = new DataContractSerializer(typeof(DiGraph<NodeType, ArcType>));
            var settings = new XmlWriterSettings { Indent = true };
            using (var w = XmlWriter.Create(file, settings))
                ds.WriteObject(w, config);
        }

        public static List<Type> getTypesFromXml(string file)
        {
            List<Type> return_types = new List<Type>();
            Dictionary<String, Type> types = new Dictionary<String, Type>() {
                { "int", Type.GetType("System.Int32") }, 
                { "string", Type.GetType("System.String") }, 
                { "float" , Type.GetType("System.Single") },
                { "double", Type.GetType("System.Double") } 
            };
            XElement xElement = XElement.Load(@file);
            String rootNodeName = xElement.Name.LocalName;
            rootNodeName = rootNodeName.Substring(9, rootNodeName.Length - 9);
            System.Console.WriteLine(rootNodeName);
            foreach (KeyValuePair<String, Type> t in types)
            {
                if (rootNodeName.Contains(t.Key))
                {
                    if (return_types.Count > 0)
                    {
                        if (rootNodeName.Substring(0, t.Key.Length) == t.Key)
                        {
                            return_types.Insert(0, t.Value);
                        }
                        else
                        {
                            return_types.Add(t.Value);
                        }
                    }
                    else
                    {
                        return_types.Add(t.Value);
                    }
                }
            }
            return return_types;
        }
    }
}
