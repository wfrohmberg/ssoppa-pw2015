﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Graph
{
    [DataContract(IsReference = true)]
    public class Arc<NodeType, ArcType> : IComparable<Arc<NodeType, ArcType>>
    {
        private Node<NodeType, ArcType> nodeFrom;
        private Node<NodeType, ArcType> nodeTo;
        private ArcType arcValue;
        private int x1, x2, y1, y2;

        [DataMember]
        public Node<NodeType, ArcType> NodeFrom
        {
            get { return nodeFrom; }
            set { nodeFrom = value; }
        }

        [DataMember]
        public Node<NodeType, ArcType> NodeTo
        {
            get { return nodeTo; }
            set { nodeTo = value; }
        }

        [DataMember]
        public ArcType ArcValue
        {
            get { return arcValue; }
            set { arcValue = value; }
        }

        [DataMember]
        public int Y2
        {
            get { return y2; }
            set { y2 = value; }
        }

        [DataMember]
        public int Y1
        {
            get { return y1; }
            set { y1 = value; }
        }

        [DataMember]
        public int X2
        {
            get { return x2; }
            set { x2 = value; }
        }

        [DataMember]
        public int X1
        {
            get { return x1; }
            set { x1 = value; }
        }

        public Arc()
        {
            setCoordinates(0, 0, 0, 0);
        }

        public Arc(ArcType arc)
        {
            this.arcValue = arc;
            setCoordinates(0, 0, 0, 0);
        }

        public Arc(ArcType arc, Node<NodeType, ArcType> nodeFrom, Node<NodeType, ArcType> nodeTo)
        {
            this.arcValue = arc;
            this.nodeFrom = nodeFrom;
            this.nodeTo = nodeTo;
            setCoordinates(0, 0, 0, 0);
        }

        public static implicit operator ArcType(Arc<NodeType, ArcType> arc)
        {
            return arc.getArcValue();
        }

        public static implicit operator Arc<NodeType, ArcType>(ArcType arc)
        {
            return new Arc<NodeType, ArcType>(arc);
        }

        public override string ToString()
        {
            return this.arcValue.ToString();
        }

        public ArcType getArcValue()
        {
            return this.arcValue;
        }

        public void setArcValue(ArcType arc)
        {
            this.arcValue = arc;
        }

        public Node<NodeType, ArcType> getNodeFrom()
        {
            return this.nodeFrom;
        }

        public Node<NodeType, ArcType> getNodeTo()
        {
            return this.nodeTo;
        }
        public void setCoordinates(int x1, int x2, int y1, int y2)
        {
            this.x1 = x1;
            this.x2 = x2;
            this.y1 = y1;
            this.y2 = y2;
        }

        public void getCoordinates(out int x1, out int x2, out int y1, out int y2)
        {
            x1 = this.x1;
            x2 = this.x2;
            y1 = this.y1;
            y2 = this.y2;
        }


        public void setNodeFrom(Node<NodeType, ArcType> node)
        {
            this.nodeFrom = node;
        }

        public void setNodeTo(Node<NodeType, ArcType> node)
        {
            this.nodeTo = node;
        }

        public int CompareTo(Arc<NodeType, ArcType> temp)
        {
            return this.nodeTo.CompareTo(temp.getNodeTo());
        }
    }
}
