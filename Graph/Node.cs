﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Graph
{
    [DataContract(IsReference=true)]
    public class Node<NodeType, ArcType> : IComparable<Node<NodeType, ArcType>>, IEnumerable<Arc<NodeType, ArcType>>, IEnumerable
    {
        private int index;
        private int x, y;
        private NodeType nodeValue;
        private List<Arc<NodeType, ArcType>> arcs;

        [DataMember]
        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        [DataMember]
        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        [DataMember]
        public int X
        {
            get { return x; }
            set { x = value; }
        }

        [DataMember]
        public NodeType NodeValue
        {
            get { return nodeValue; }
            set { nodeValue = value; }
        }

        [DataMember]
        public List<Arc<NodeType, ArcType>> Arcs
        {
            get { return arcs; }
            set { arcs = value; }
        }

        public Node()
        {
            this.arcs = new List<Arc<NodeType, ArcType>>(arcs);
            setCoordinates(0, 0);
        }

        public Node(int index, NodeType vertex, Arc<NodeType, ArcType>[] arcs)
        {
            this.index = index;
            this.nodeValue = vertex;
            this.arcs = new List<Arc<NodeType, ArcType>>(arcs);
            setCoordinates(0, 0);
        }

        public Node(int index, NodeType vertex)
        {
            this.index = index;
            this.nodeValue = vertex;
            this.arcs = new List<Arc<NodeType, ArcType>>();
            setCoordinates(0, 0);
        }

        public Node(NodeType vertex)
        {
            this.nodeValue = vertex;
            this.arcs = new List<Arc<NodeType, ArcType>>();
            setCoordinates(0, 0);
        }

        public static implicit operator NodeType(Node<NodeType, ArcType> node)
        {
            return node.getNodeValue();
        }

        public static implicit operator Node<NodeType, ArcType>(NodeType node)
        {
            return new Node<NodeType,ArcType>(node);
        }

        public void AddToArcs(Arc<NodeType, ArcType> arc)
        {
            this.arcs.Add(arc);
        }

        public int getIndex()
        {
            return this.index;
        }

        public void setIndex(int index)
        {
            this.index = index;
        }

        public override string ToString()
        {
            return this.nodeValue.ToString();
        }

        public NodeType getNodeValue()
        {
            return this.nodeValue;
        }

        public void setNodeValue(NodeType node)
        {
            this.nodeValue = node;
        }

        public void setCoordinates(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public void getCoordinates(out int x, out int y)
        {
            x = this.x;
            y = this.y;
        }

        public List<Arc<NodeType, ArcType>> getArcs()
        {
            return this.arcs;
        }

        public bool Equals(Node<NodeType, ArcType> temp)
        {
            return this.index == temp.getIndex();
        }

        public int CompareTo(Node<NodeType, ArcType> temp)
        {
            return this.index < temp.getIndex() ? -1 : 1;
        }

        public void Add(object o)
        {
            arcs.Add(o as Arc<NodeType, ArcType>);
        }

        public IEnumerator<Arc<NodeType, ArcType>> GetEnumerator()
        {
            foreach (Arc<NodeType, ArcType> temp in this.arcs)
            {
                yield return temp;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
