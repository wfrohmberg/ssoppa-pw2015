﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfApplication1
{
    public class DrawableDiGraph
    {
        public DrowableNode selectedNode = null;
        public bool addArcClicked = false;
        //public bool nodeSelected = false;

        public List<DrowableNode> nodes = new List<DrowableNode>();
        public List<DrowableArc> arcs = new List<DrowableArc>();
        private Canvas myCanvas;

        private static DrawableDiGraph instance;

         public static DrawableDiGraph Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new DrawableDiGraph();
                }
                return instance;
            }
        }

        public void setCanvas(Canvas canvas)
        {
            this.myCanvas = canvas;
        }

        public void drawGraph() {
            DrowableNode n1 = new DrowableNode(myCanvas, 100, 100);
            DrowableNode n2 = new DrowableNode(myCanvas, 200, 150);
            nodes.Add(n1); nodes.Add(n2);
            n1.removedNode += this.onNodeRemoved;
            n2.removedNode += this.onNodeRemoved;

            n1.selectedNode += this.onSelectedNode;
            n2.selectedNode += this.onSelectedNode;

            DrowableArc a1 = new DrowableArc(myCanvas, n1.getX(), n1.getY(), n2.getX(), n2.getY());
            a1.removedArc += this.onArcRemoved; //adding pointer to method
            arcs.Add(a1);
            a1.addNodes(n1, n2);
            n1.addArc(a1);
            n2.addArc(a1);

            DrowableNode n3 = new DrowableNode(myCanvas, 300, 250);
            nodes.Add(n3);
            n3.removedNode += this.onNodeRemoved;
            n3.selectedNode += this.onSelectedNode;

            DrowableArc a2 = new DrowableArc(myCanvas, n2.getX(), n2.getY(), n3.getX(), n3.getY());
            a2.removedArc += this.onArcRemoved; //adding pointer to method
            arcs.Add(a2);
            a2.addNodes(n2, n3);
            n2.addArc(a2);
            n3.addArc(a2);

            DrowableNode n4 = new DrowableNode(myCanvas, 200, 250);
            n4.removedNode += this.onNodeRemoved;
            n4.selectedNode += this.onSelectedNode;
            nodes.Add(n4);

            DrowableArc a3 = new DrowableArc(myCanvas, n2.getX(), n2.getY(), n4.getX(), n4.getY());
            a3.removedArc += this.onArcRemoved; //adding pointer to method
            arcs.Add(a3);
            a3.addNodes(n2, n4);
            n2.addArc(a3);
            n4.addArc(a3);
        }

        private DrawableDiGraph() { }
        public void onArcRemoved(object source, EventArgs e)
        {
            int i = arcs.IndexOf(source as DrowableArc);
            arcs.Remove(source as DrowableArc);
            //removeArc(this);
            Console.WriteLine("I am removing arc from list");
        }

        public void clearGraph()
        {
            int i = 0;
            int arcCount = arcs.Count;
            for (i = 0; i < arcCount; i++){
                myCanvas.Children.Remove(this.arcs[0].line);
                arcs.Remove(arcs[0]);
            }

            int nodeCount = nodes.Count;
            for (i = 0; i < nodeCount; i++)
            {
                myCanvas.Children.Remove(this.nodes[0].ellipse);
                nodes.Remove(nodes[0]);
            }            
        }

        public void onNodeRemoved(object source, EventArgs e)
        {
            DrowableNode node = source as DrowableNode;
            foreach (DrowableArc arc in node.arcs)
            {
                arcs.Remove(arc);
            }
            nodes.Remove(node);
        }

        public void setCoordinates()
        {
            dynamic d = MainWindow.dataProvider;//MaidataProvider;

            for(int i = 0; i < nodes.Count; i++){
                DrowableNode n = nodes[i];
                d[i].setCoordinates((int)n.getX(), (int)n.getY());     
            }

            int maxCount = 0;
            for (int i = 0; ((i < d.numberOfArcs()) && (maxCount < d.numberOfArcs())); i++)
            {
                int count;
                try
                {
                    // Do not initialize this variable here.
                    count = d.arcs[i].Count;
                    for (int j = 0; j < count; j++)
                    {
                        int nodeFromIndex = d.arcs[i][j].getNodeFrom().getIndex();
                        int nodeToIndex = d.arcs[i][j].getNodeTo().getIndex();

                        DrowableArc a = arcs[maxCount];
                        d[nodeFromIndex, nodeToIndex].setCoordinates((int)a.n1.getX(), (int)a.n2.getX(), (int)a.n1.getY(), (int)a.n2.getY());
                        maxCount++;
                    }
                }
                catch
                {

                }
                
            }
        }

        public int indexOfNode(DrowableNode node){
            return nodes.IndexOf(node);
        }

        public int indexOfArc(DrowableArc arc)
        {
            return arcs.IndexOf(arc);
        }

        public void onSelectedNode(object source, EventArgs e)
        {
            DrowableNode node = source as DrowableNode;
            if (addArcClicked)
            {
                if (this.selectedNode == null)
                {
                    this.selectedNode = node;
                }
                else
                {
                    int nodeFromIndex = this.indexOfNode(selectedNode);
                    int nodeToIndex = this.indexOfNode(node);
                    DrowableArc a = new DrowableArc(myCanvas, selectedNode.getX(), selectedNode.getY(), node.getX(), node.getY());
                    a.removedArc += this.onArcRemoved; //adding pointer to method
                    arcs.Add(a);
                    a.addNodes(selectedNode, node);
                    selectedNode.addArc(a);
                    node.addArc(a);

                    dynamic d = MainWindow.dataProvider;

                     switch (MainWindow.arcType.ToString())
                    {
                        case "System.Int32":
                            d[nodeFromIndex, nodeToIndex] = 0;
                             break;
                        case "System.Double":
                             d[nodeFromIndex, nodeToIndex] = 0.0;
                            break;
                        case "System.Single":
                             d[nodeFromIndex, nodeToIndex] = 0.0;
                            break;
                        case "System.String":
                            d[nodeFromIndex, nodeToIndex] = "Empty string";
                            break;
                     }
                    //selectedNode._isRectDragInProg = false;
                    //node._isRectDragInProg = false;
                }
            }
            //removeArc(this);
        }
    }
}
