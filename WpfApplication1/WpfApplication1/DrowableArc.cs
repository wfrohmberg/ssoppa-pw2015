﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace WpfApplication1
{
    public class DrowableArc
    {
        public Line line;
        public DrowableNode n1;
        public DrowableNode n2;
        public Canvas myCanvas;
        private ArcForm arcForm;

        public delegate void removeArcEventHandler(object source, EventArgs args);
        public event removeArcEventHandler removedArc;
        protected virtual void OnRemovedArc()
        {
            if (removedArc != null)
            {
                removedArc(this, EventArgs.Empty);
            }
        }

        public DrowableArc(Canvas aCanvas, double x1, double y1, double x2, double y2)
        {
            myCanvas = aCanvas;

            line = new Line();

            line.MouseLeftButtonDown += new MouseButtonEventHandler(line_MouseUp);
            line.MouseLeave += new MouseEventHandler(line_MouseLeave);
            line.MouseEnter += new MouseEventHandler(line_MouseEnter);

            line.Stroke = Brushes.Black;
            line.StrokeStartLineCap = PenLineCap.Round;
            line.StrokeEndLineCap = PenLineCap.Triangle;
            line.StrokeThickness = 10;
            line.X1 = x1; line.X2 = x2;
            line.Y1 = y1; line.Y2 = y2;

            myCanvas.Children.Add(this.line);
        }

        public void addNodes(DrowableNode n1, DrowableNode n2)
        {
            this.n1 = n1;
            this.n2 = n2; 
        }
        void line_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount < 2)
            {
                this.line.Stroke = Brushes.Red;
            }
            else
            {
                this.line.Stroke = Brushes.Black;
                int index = DrawableDiGraph.Instance.indexOfArc(this);
                int nodeFromIndex = DrawableDiGraph.Instance.indexOfNode(DrawableDiGraph.Instance.arcs[index].n1);
                int nodeToIndex = DrawableDiGraph.Instance.indexOfNode(DrawableDiGraph.Instance.arcs[index].n2);
                using (arcForm = new ArcForm(nodeFromIndex, nodeToIndex))
                {
                    arcForm.Text = "Arc Information";
                    arcForm.acceptArc += this.onAccept;
                    arcForm.removedArc += this.onRemove;
                    arcForm.ShowDialog();
                }
            }
        }

        public void onAccept(object source, EventArgs e)
        {
            ArcForm arc = source as ArcForm;

            dynamic d = MainWindow.dataProvider;
            String type = MainWindow.arcType.ToString();
            switch (type)
            {
                case "System.Int32":
                    d[arc.nodeFromIndex, arc.nodeToIndex] = Convert.ToInt32(arc.t.Text);
                    break;
                case "System.Double":
                    d[arc.nodeFromIndex, arc.nodeToIndex] = Convert.ToDouble(arc.t.Text);
                    break;
                case "System.Single":
                    d[arc.nodeFromIndex, arc.nodeToIndex] = Convert.ToSingle(arc.t.Text);
                    break;
                case "System.String":
                    d[arc.nodeFromIndex, arc.nodeToIndex] = arc.t.Text;
                    break;
            }
            this.arcForm.Close();
        }

        public void onRemove(object source, EventArgs e)
        {
            ArcForm arc = source as ArcForm;

            dynamic d = MainWindow.dataProvider;
            d = d - d[arc.nodeFromIndex, arc.nodeToIndex];

            myCanvas.Children.Remove(this.line);
            OnRemovedArc(); //call event
            arcForm.Close();
        }

        void line_MouseEnter(object sender, EventArgs e)
        {
            this.line.Stroke = Brushes.Red;
        }

        private void line_MouseLeave(object sender, EventArgs e)
        {
            // Update the mouse event label to indicate the MouseLeave event occurred.
            //label1.Text = sender.GetType().ToString() + ": MouseLeave";
            this.line.Stroke = Brushes.Black;
        }
    }
}
