﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;

namespace WpfApplication1
{
    public class DrowableNode
    {
        public Ellipse ellipse;
        private Canvas canvas;
        public List<DrowableArc> arcs;
        public const int SIZE = 50;
        public NodeForm nodeForm;

        public delegate void removeNodeEventHandler(object source, EventArgs args);
        public event removeNodeEventHandler removedNode;

        public delegate void selectNodeEventHandler(object source, EventArgs args);
        public event selectNodeEventHandler selectedNode;
        protected virtual void OnRemovedNode()
        {
            if (removedNode != null)
            {
                removedNode(this, EventArgs.Empty);
            }
        }
        protected virtual void OnSelectedNode()
        {
            if (selectedNode != null)
            {
                selectedNode(this, EventArgs.Empty);
            }
        }

        public DrowableNode(Canvas aCanvas, float x, float y)
        {
            canvas = aCanvas;
            arcs = new List<DrowableArc>();
            
            ellipse = new Ellipse();

            ellipse.Stroke = System.Windows.Media.Brushes.Orange;
            ellipse.Fill = System.Windows.Media.Brushes.Blue;
            ellipse.StrokeThickness = 3;
            ellipse.Width = SIZE;
            ellipse.Height = SIZE;

            ellipse.MouseLeftButtonDown += new MouseButtonEventHandler(node_MouseLeftButtonDown);
            ellipse.MouseLeftButtonUp   += new MouseButtonEventHandler(node_MouseLeftButtonUp);
            ellipse.MouseMove += new MouseEventHandler(node_MouseMove); 

            aCanvas.Children.Add(ellipse);
            this.setPosition(x, y);
        }

        public void addArc(DrowableArc a)
        {
            this.arcs.Add(a);
        }

        public double getX()
        {
            return Canvas.GetLeft(this.ellipse) + SIZE/2;
        }

        public double getY()
        {
            return Canvas.GetTop(this.ellipse) + SIZE/2; 
        }

        public void setPosition(double x, double y)
        {
            Canvas.SetLeft(this.ellipse, x - SIZE / 2);
            Canvas.SetTop(this.ellipse, y - SIZE / 2);
        }

        public bool _isRectDragInProg = false;

        private void node_MouseLeftButtonDown( object sender, MouseButtonEventArgs e )
        {
            if (e.ClickCount < 2)
            {
                _isRectDragInProg = true;
                this.ellipse.CaptureMouse();
                OnSelectedNode();
            }
            else
            {
                int index = DrawableDiGraph.Instance.indexOfNode(this);
                using (nodeForm = new NodeForm(index))
                {
                    nodeForm.Text = "Node Information";
                    nodeForm.acceptNode += this.onAccept;
                    nodeForm.removedNode += this.onRemove;
                    nodeForm.ShowDialog();
                }
            }
        }

        public void onRemove(object source, EventArgs e)
        {
            int index = DrawableDiGraph.Instance.indexOfNode(this);
            dynamic d = MainWindow.dataProvider;
            
            //first arcs
            foreach (DrowableArc arc in arcs)
            {
                canvas.Children.Remove(arc.line);
                int nodeFromIndex = DrawableDiGraph.Instance.indexOfNode(arc.n1);
                int nodeToIndex = DrawableDiGraph.Instance.indexOfNode(arc.n2);
                d = d - d[nodeFromIndex, nodeToIndex];
            }
            //second node
            canvas.Children.Remove(this.ellipse);
            d = d - d[index];

            int f = d.numberOfNodes();
            int g = d.numberOfArcs();

            OnRemovedNode(); //call event
            nodeForm.Close();
        }

        public void onAccept(object source, EventArgs e)
        {
            NodeForm node = source as NodeForm;

            dynamic d = MainWindow.dataProvider;
            String type = MainWindow.nodeType.ToString();
            switch (type)
            {
                case "System.Int32":
                    d[node.nodeId] = Convert.ToInt32(node.t.Text);
                    break;
                case "System.Double":
                    d[node.nodeId] = Convert.ToDouble(node.t.Text);
                    break;
                case "System.Single":
                    d[node.nodeId] = Convert.ToSingle(node.t.Text);
                    break;
                case "System.String":
                    d[node.nodeId] = node.t.Text;
                    break;
            }
            nodeForm.Close();
        }

        private void node_MouseLeftButtonUp( object sender, MouseButtonEventArgs e )
        {
            _isRectDragInProg = false;
            this.ellipse.ReleaseMouseCapture();
        }

        private void node_MouseMove( object sender, MouseEventArgs e )
        {
            if( !_isRectDragInProg ) return;

            // get the position of the mouse relative to the Canvas
            var mousePos = e.GetPosition(canvas);

            double x = mousePos.X;
            double y = mousePos.Y;

            this.setPosition(x, y);

            refreshArcsPosition(x, y);
            
        }

        public void refreshArcsPosition(double x, double y)
        {
            foreach (DrowableArc arc in this.arcs)
            {
                if (Object.ReferenceEquals(this.ellipse, arc.n1.ellipse) == true)
                {
                    arc.line.X1 = x;
                    arc.line.Y1 = y;
                }
                else
                {
                    arc.line.X2 = x;
                    arc.line.Y2 = y;
                }
            }
        }


        public DrowableNode self { get; set; }
    }
}
