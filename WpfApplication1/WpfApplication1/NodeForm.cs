﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WpfApplication1
{
    public partial class NodeForm : Form
    {

        public delegate void removeEventHandler(object source, EventArgs args);
        public event removeEventHandler removedNode;

        public delegate void acceptEventHandler(object source, EventArgs args);
        public event acceptEventHandler acceptNode;
        protected virtual void OnRemove()
        {
            if (removedNode != null)
            {
                removedNode(this, EventArgs.Empty);
            }
        }
        protected virtual void OnAccept()
        {
            if (acceptNode != null)
            {
                acceptNode(this, EventArgs.Empty);
            }
        }

        public Label l;
        public TextBox t;
        public int nodeId;
        public NodeForm(int id)
        {
            nodeId = id;

            InitializeComponent();
            String type = MainWindow.nodeType.ToString();
            
             switch (type)
             {
                case "System.Int32":
                     type = "Int";
                     break;
                case "System.Double":
                     type = "Double";
                    break;
                case "System.Single":
                     type = "Float";
                    break;
                case "System.String":
                    type = "String";
                    break;
             }

                dynamic d = MainWindow.dataProvider;
                String str = d[nodeId].ToString();
                drawLabel(12, 21, type);
                drawTextBox(127, 18, str);
                setAcceptButton(15, 46);
                setRemoveButton(128, 46);

                this.ClientSize = new System.Drawing.Size(300, 100);

            this.removeButton.MouseClick += new System.Windows.Forms.MouseEventHandler(removeButton_MouseDown);
            this.acceptButton.MouseClick += new System.Windows.Forms.MouseEventHandler(acceptButton_MouseDown);
        }

        private void removeButton_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            OnRemove();
        }

        private void acceptButton_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            OnAccept();
        }


        public void drawButton() {

        }

        public void setAcceptButton(int x, int y)
        {
            this.acceptButton = new System.Windows.Forms.Button();
            this.acceptButton.Location = new System.Drawing.Point(x, y);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(75, 23);
            this.acceptButton.TabIndex = 0;
            this.acceptButton.Text = "Accept";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.Controls.Add(this.acceptButton);
        }

        public void setRemoveButton(int x, int y)
        {
            this.removeButton = new System.Windows.Forms.Button();
            this.removeButton.Location = new System.Drawing.Point(x, y);
            //this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 0;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;

            this.Controls.Add(this.removeButton);
        }

        public String getValue() {
            String value = t.Text;
            return value;
        }
        public void drawLabel(int x, int y, string text)
        {
            this.l = new Label();
            l.AutoSize = true;
            l.Location = new System.Drawing.Point(x, y);
            l.Size = new System.Drawing.Size(109, 13);
            l.Text = text;

            this.Controls.Add(l);
        }
        public void drawTextBox(int x, int y, string text)
        {
            this.t = new TextBox();
            t.Location = new System.Drawing.Point(x, y);
            t.Name = "textBox1";
            t.Size = new System.Drawing.Size(149, 20);
            t.Text = text;

            this.Controls.Add(t);
        }

        public void changeWindowsSize() {

        }

    }
}
