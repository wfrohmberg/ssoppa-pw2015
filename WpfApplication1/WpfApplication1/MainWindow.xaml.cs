﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Graph;

namespace WpfApplication1
{
    public partial class MainWindow : Window
    {
 
        public static Type nodeType = null;
        public static Type arcType = null;
        public static String dllPath = null;

        public static object dataProvider;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void myCanvas_Loaded(object sender, RoutedEventArgs e)
        {
             initGui();
            DrawableDiGraph.Instance.setCanvas(myCanvas);
            DrawableDiGraph.Instance.drawGraph();
            nodeType = Type.GetType("System.Int32");
            arcType = Type.GetType("System.String");
            Type d1 = typeof(DiGraph<,>);
            Type[] typeArgs = { nodeType, arcType };
            Type constructed = d1.MakeGenericType(typeArgs);
            dataProvider = Activator.CreateInstance(constructed);

             dynamic d = dataProvider;
             int i = d.numberOfNodes();
             d[0] = 23;
             d[1] = 2;
             d[2] = 32;
             d[3] = 3;
             //d = d - d[1];   

             d[0, 1] = "sd";
             d[1, 2] = "sd";
             d[1, 3] = "we";

             /*d = d - d[0, 1];
             d = d - d[1, 2];
             d = d - d[1, 3];*/

             int f = d.numberOfNodes();
             int ai = getCentralPointIndex();
        }

        private int getCentralPointIndex()
        {
            dynamic d = dataProvider;
            int count = d.numberOfNodes();

            int[] incomingArcs = new int[count];
            int[] outcomingArcs = new int[count];

            for (int i = 0; i < count; i++)
            {
                int arcCount = d[i].getArcs().Count;
                for (int j = 0; j < arcCount; j++)
                {
                    int nodeToIndex = d[i].getArcs()[j].getNodeTo().getIndex();
                    int nodeFromIndex = d[i].getArcs()[j].getNodeFrom().getIndex();

                    incomingArcs[nodeToIndex] += 1;
                    outcomingArcs[nodeFromIndex] += 1;
                }
            }
            int[] nodeScore = new int[count];
            for (int i = 0; i < count; i++)
            {
                nodeScore[i] = incomingArcs[i] + outcomingArcs[i];
                int a = nodeScore[i]; 
            }

            int maxValue = nodeScore.Max();
            return nodeScore.ToList().IndexOf(maxValue);
        }

        private void squareOrder()
        {
            dynamic d = dataProvider;
            int sideLength = (int)Math.Ceiling(Math.Sqrt(d.numberOfNodes()));

            double canvasWidth = myCanvas.Width;
            double canvasHeight = myCanvas.Height;
            int stepHeight = (int)canvasWidth/sideLength - 30;
            int stepWidth = (int)canvasHeight/sideLength - 30;

            int step = Math.Min(stepHeight, stepWidth); 

            int tempXObjectPosition = 30;
            int tempYObjectPosition = 30;
            int count = 0;

            foreach (DrowableNode n in DrawableDiGraph.Instance.nodes)
            {
                n.setPosition((double)tempXObjectPosition, (double)tempYObjectPosition);
                n.refreshArcsPosition((double)tempXObjectPosition, (double)tempYObjectPosition);

                count++;
                if (count >= sideLength)
                {
                    count = 0;
                    tempXObjectPosition = 30;
                    tempYObjectPosition += step;
                }
                else
                {
                    tempXObjectPosition += step;
                }
            }
        }


        private void circleOrderButton_Click(object sender, RoutedEventArgs e)
        {
            float i = DrawableDiGraph.Instance.nodes.Count;
            float step = (float)360.0 / i;
            float iterator = 0;
            foreach (DrowableNode n in DrawableDiGraph.Instance.nodes)
            {
                float radius = 100;//(float)myCanvas.Height/2;
                PointF point = this.PointOnCircle(radius, iterator, new PointF((float)(myCanvas.Width) / 2, (float)(myCanvas.Height) / 2));

                n.setPosition((double)point.X, (double)point.Y);
                n.refreshArcsPosition((double)point.X, (double)point.Y);
                iterator += step;
            }
        }

        private void heightNodeInCircle() {
            int heightNodeIndex = getCentralPointIndex();

            float i = DrawableDiGraph.Instance.nodes.Count-1;
            float step = (float)360.0 / i;
            float iterator = 0;
            foreach (DrowableNode n in DrawableDiGraph.Instance.nodes)
            {
                if(n == DrawableDiGraph.Instance.nodes[heightNodeIndex]){
                    n.setPosition(myCanvas.Width / 2, myCanvas.Height / 2);
                    n.refreshArcsPosition(myCanvas.Width / 2, myCanvas.Height / 2);
                } else {
                    float radius = 100;//(float)myCanvas.Height/2;
                    PointF point = this.PointOnCircle(radius, iterator, new PointF((float)(myCanvas.Width) / 2, (float)(myCanvas.Height) / 2));

                    n.setPosition((double)point.X, (double)point.Y);
                    n.refreshArcsPosition((double)point.X, (double)point.Y);
                    iterator += step;
                }
            }
        }


        public PointF PointOnCircle(float radius, float angleInDegrees, PointF origin)
        {
            // Convert from degrees to radians via multiplication by PI/180        
            float x = (float)(radius * Math.Cos(angleInDegrees * Math.PI / 180F)) + origin.X;
            float y = (float)(radius * Math.Sin(angleInDegrees * Math.PI / 180F)) + origin.Y;

            return new PointF(x, y);
        }

        private void initGui() {
            this.nodeComboList.Items.Add("Int");
            this.nodeComboList.Items.Add("String");
            this.nodeComboList.Items.Add("Float");
            this.nodeComboList.Items.Add("Double");

            this.arcComboList.Items.Add("Int");
            this.arcComboList.Items.Add("String");
            this.arcComboList.Items.Add("Float");
            this.arcComboList.Items.Add("Double");
        }

        private void AddNode_Click(object sender, RoutedEventArgs e)
        {
            DrowableNode n = new DrowableNode(myCanvas, 100, 100);
            n.removedNode += DrawableDiGraph.Instance.onNodeRemoved;
            n.selectedNode += DrawableDiGraph.Instance.onSelectedNode;
            DrawableDiGraph.Instance.nodes.Add(n);

            dynamic d = MainWindow.dataProvider;

            int nodeIndex = DrawableDiGraph.Instance.indexOfNode(n);

            switch (MainWindow.nodeType.ToString())
            {
                case "System.Int32":
                    d[nodeIndex] = 0;
                    break;
                case "System.Double":
                    d[nodeIndex] = 0.0;
                    break;
                case "System.Single":
                    d[nodeIndex] = 0;
                    break;
                case "System.String":
                    d[nodeIndex] = "Empty string";
                    break;
            }
        }

        public static void acceptedNode(DrowableNode node, List<String> values) {
  
        }

        private void AddArc_Click(object sender, RoutedEventArgs e)
        {
            if (DrawableDiGraph.Instance.addArcClicked == false)
            {
                DrawableDiGraph.Instance.addArcClicked = true;
                AddArc.Background = System.Windows.Media.Brushes.Green;
            }
            else
            {
                DrawableDiGraph.Instance.addArcClicked = false;
                DrawableDiGraph.Instance.selectedNode = null;
                AddArc.ClearValue(Button.BackgroundProperty);
            }
        }


        private void acceptSelectionButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.arcComboList.SelectedIndex > -1)
            {
                String type = this.arcComboList.SelectedValue as String;
                this.arcTypeLabel.Content = type;
                arcType = returnType(type);
            }

            if (this.nodeComboList.SelectedIndex > -1)
            {
                String type = this.nodeComboList.SelectedValue as String;
                this.nodeTypeLabel.Content = type;
                nodeType = returnType(type);
            }

            Type d1 = typeof(DiGraph<,>);
            Type[] typeArgs = { nodeType, arcType };
            Type constructed = d1.MakeGenericType(typeArgs);
            dataProvider = Activator.CreateInstance(constructed);

            DrawableDiGraph.Instance.clearGraph();
        }

        private Type returnType(String type)
        {
            String _type = "";
            if (type == "Int")
            {
                _type = "System.Int32";
            }
            else if (type == "String")
            {
                _type = "System.String";
            }
            else if (type == "Double")
            {
                _type = "System.Double";
            }
            else if (type == "Float")
            {
                _type = "System.Single";
            }
            return Type.GetType(_type);
        }

        

        private void saveToXML_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog ofd = new SaveFileDialog();

            ofd.Filter = "XML|*.xml";
            bool? dialogResult = ofd.ShowDialog();
            if (dialogResult.HasValue && dialogResult.Value)
            {
                string xmlPath = ofd.FileName;

                DrawableDiGraph.Instance.setCoordinates();

                dynamic d = dataProvider;

                Type d1 = typeof(DiGraph<,>);
                Type[] typeArgs = { nodeType, arcType };
                Type constructed = d1.MakeGenericType(typeArgs);

                object[] parameters = { xmlPath, d };
                constructed.GetMethod("serializeToXml").Invoke(null, parameters);
            }
        }

        private void loadFromXML_Click(object sender, RoutedEventArgs e)
        {

            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter = "XML|*.xml";
            bool? dialogResult = ofd.ShowDialog();
            if (dialogResult.HasValue && dialogResult.Value)
            {
                string xmlPath = ofd.FileName;

                Type d1 = typeof(DiGraph<,>);
                Type[] typeArgs = { nodeType, arcType };
                Type constructed = d1.MakeGenericType(typeArgs);

                string[] parameters = { xmlPath };

                List<Type> types = constructed.GetMethod("getTypesFromXml").Invoke(null, parameters) as List<Type>;

                nodeType = types[0];
                arcType = types[1];

                Type[] typeArgs1 = { nodeType, arcType };
                Type constructed1 = d1.MakeGenericType(typeArgs1);
                
                dataProvider = constructed1.GetMethod("xmlToGraph").Invoke(null, parameters);

                nodeType = dataProvider.GetType().GetGenericArguments()[0];
                arcType = dataProvider.GetType().GetGenericArguments()[1];

                DrawableDiGraph.Instance.clearGraph();

                dynamic d = dataProvider;

                for (int i = 0; i < d.numberOfNodes(); i++)
                {
                    int x;
                    int y;
                    d[i].getCoordinates(out x, out y);
                    DrowableNode n = new DrowableNode(myCanvas, x, y);
                    n.removedNode += DrawableDiGraph.Instance.onNodeRemoved;
                    n.selectedNode += DrawableDiGraph.Instance.onSelectedNode;
                    DrawableDiGraph.Instance.nodes.Add(n);

                    int nodeIndex = DrawableDiGraph.Instance.indexOfNode(n);
                }

                int maxCount = 0;
                for (int i = 0; ((i < d.numberOfArcs()) && (maxCount < d.numberOfArcs() )); i++)
                {
                    try
                    {
                        int count = d.arcs[i].Count;
                        for (int j = 0; j < count; j++)
                        {
                            int nodeFromIndex = d.arcs[i][j].getNodeFrom().getIndex();
                            int nodeToIndex = d.arcs[i][j].getNodeTo().getIndex();

                            DrowableArc a = new DrowableArc(myCanvas, DrawableDiGraph.Instance.nodes[nodeFromIndex].getX(),
                                                                      DrawableDiGraph.Instance.nodes[nodeFromIndex].getY(),
                                                                      DrawableDiGraph.Instance.nodes[nodeToIndex].getX(),
                                                                      DrawableDiGraph.Instance.nodes[nodeToIndex].getY());
                            a.removedArc += DrawableDiGraph.Instance.onArcRemoved;//this.onArcRemoved; //adding pointer to method
                            DrawableDiGraph.Instance.arcs.Add(a);
                            a.addNodes(DrawableDiGraph.Instance.nodes[nodeFromIndex], DrawableDiGraph.Instance.nodes[nodeToIndex]);
                            DrawableDiGraph.Instance.nodes[nodeFromIndex].addArc(a);
                            DrawableDiGraph.Instance.nodes[nodeToIndex].addArc(a);

                            maxCount++;
                        }
                    }
                    catch
                    {

                    }
                    
                    
                    
                }


            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.squareOrder();
        }

        private void hightNodeIn_Click(object sender, RoutedEventArgs e)
        {
            heightNodeInCircle();
        }

    }
}
